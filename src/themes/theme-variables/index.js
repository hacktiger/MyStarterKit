import { Dimensions } from 'react-native';

export const DEVICE_WIDTH = Dimensions.get('window').width;
export const DEVICE_HEIGHT = Dimensions.get('window').height;

export const COLORS = {
    lightGreen: '#37C892',
    red: '#f44336',
    grey: '#666666'
};

export const TEXT = {
    i: {
        fontStyle: 'italic'
    }
};

export const FLEX_BOX = {
    absolute: {
        position: 'absolute'
    },
    'flex-1': {
        flex: 1
    },
    fg: {
        flexGrow: 1
    },
    fr: {
        flexDirection: 'row'
    },
    frr: {
        flexDirection: 'row-reverse'
    },
    fcr: {
        flexDirection: 'column-reverse'
    },
    fw: {
        flexWrap: 'wrap'
    },
    aifs: {
        alignItems: 'flex-start'
    },
    aic: {
        alignItems: 'center'
    },
    aife: {
        alignItems: 'flex-end'
    },
    asfs: {
        alignSelf: 'flex-start'
    },
    asc: {
        alignSelf: 'center'
    },
    asfe: {
        alignSelf: 'flex-end'
    },
    ass: {
        alignSelf: 'stretch'
    },
    jcfs: {
        justifyContent: 'flex-start'
    },
    jcfe: {
        justifyContent: 'flex-end'
    },
    jcc: {
        justifyContent: 'center'
    },
    jcsb: {
        justifyContent: 'space-between'
    },
    jcsa: {
        justifyContent: 'space-around'
    }
}