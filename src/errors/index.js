const client_errors = [

];

const server_errors = [

];

const network_errors = [

];

const errors = {
    client_errors,
    server_errors,
    network_errors
};

export default errors;
