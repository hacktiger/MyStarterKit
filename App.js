/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { build, connectTheme } from 'themes';
import { COLORS, TEXT, FLEX_BOX } from 'themes/theme-variables';
import PinCode from 'components/PinCode';

build({ 
    colors: COLORS,
    styles: [
        TEXT,
        FLEX_BOX
    ]
}, StyleSheet);

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View cls='bg-red'>
            <Text cls={['lightGreen', { 'bg-grey': true }]}>Welcome to React Native!</Text>
        </View>
        <Text cls={['lightGreen bg-red i', { 'bg-grey': false }]}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
        <View cls='fr flex-1'>
            <View cls='flex-1 bg-lightGreen aic'>
                <Text>1234</Text>
            </View>
            <View cls='flex-1 bg-red jcc'>
                <Text>1234</Text>
            </View>
            <PinCode />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default connectTheme(App);
